# John Plazarte

## Ejecución
Angular: debe abrir el proyecto con Visual Studio
1.	Abrir directorio del proyecto angular.
2.	Ejecutar npm install para crear node_modules a partir del package.json.
3.	Ejecutar proyecto con ng serve -o, abrirá el navegador automáticamente, caso contrario ingresar http://localhost:4200.
- Nota: debe tener en ejecución la aplicación backend.


## Librerias usadas
- Boostrap
- jquery
- popper.js 
- ngx-pagination (para paginación de la lista)
