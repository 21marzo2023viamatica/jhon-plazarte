import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AreaService {

  private urlApi="http://localhost:8080/area";

  constructor(private http:HttpClient) { }

  public listar(): Observable<any>{
    return this.http.get<any>(this.urlApi);
  }
}
