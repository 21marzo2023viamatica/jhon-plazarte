import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Empleado } from 'src/app/components/empleado-nuevo/Empleado';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {
  private urlApi="http://localhost:8080/empleado";

  constructor(private http:HttpClient) { }

  public listar(): Observable<any>{
    return this.http.get<any>(this.urlApi);
  }
  public obtenerEmpleado(id:number): Observable<Empleado>{
    return this.http.get<Empleado>(this.urlApi+"/"+id);
  }

  public guardar(empleado:Empleado): Observable<Object>{
    return this.http.post<Object>(this.urlApi,empleado);
  }

  public eliminar(id:number):Observable<any>{
    return this.http.delete(this.urlApi+"/"+id);
  }
}
