import { Component } from '@angular/core';
import { AreaService } from 'src/app/services/Area/area.service';
import { RolService } from 'src/app/services/Rol/rol.service';
import { Router } from '@angular/router';
import { EmpleadoService } from 'src/app/services/Empleado/empleado.service';
import { Empleado } from './Empleado';
import { Rol } from './Rol';
import { Area } from './Area';

@Component({
  selector: 'app-empleado-nuevo',
  templateUrl: './empleado-nuevo.component.html',
  styleUrls: ['./empleado-nuevo.component.css']
})
export class EmpleadoNuevoComponent {

  nombre?='';
  apellidos?='';
  fechaNace?='';
  correo?='';
  contrasena?='';
  rol?=1;
  area?=1;

  roles:any[]=[];
  areas:any[]=[];

  empleado: Empleado;
  unRol:Rol;
  unaArea:Area;

  faltanDatos=false;

  constructor(private apiRol:RolService,private apiArea:AreaService,private router:Router,private apiEmpleado:EmpleadoService){
    this.empleado = new Empleado();
    this.unRol=new Rol();
    this.unaArea=new Area();
  }

  ngOnInit(): void {
    this.listarRoles();
    this.listarAreas();
  }

  guardarEmpleado(){
    if(this.datosFaltantes()){
      this.faltanDatos=true;
    }else{
      this.faltanDatos=false;

      this.unRol.id=this.rol;
      this.unaArea.id=this.area;

      this.empleado.nombre=this.nombre;
      this.empleado.apellidos=this.apellidos;
      this.empleado.fechaNace=this.fechaNace;
      this.empleado.correo=this.correo;
      this.empleado.contrasena=this.contrasena;
      this.empleado.rol=this.unRol;
      this.empleado.area=this.unaArea;
      this.apiEmpleado.guardar(this.empleado).subscribe(data => {
        this.router.navigate(['']);
      }
      );
    }
    

  }

  listarAreas(){
    this.apiArea.listar().subscribe(data=>{
      this.areas=data;
    });
  }

  listarRoles(){
    this.apiRol.listar().subscribe(data=>{
      this.roles=data;
    });
  }
  datosFaltantes(){
    if(this.nombre==='' || this.apellidos===''||this.fechaNace===''||this.correo===''||this.contrasena===''){
      return true;
    }
    return false;
  }
}
