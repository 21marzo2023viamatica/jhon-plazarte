import { Area } from "./Area";
import { Rol } from "./Rol";

export class Empleado {
    id?: number;
    nombre?: string;
    apellidos?: string;
    fechaNace?: string;
    correo?:string;
    contrasena?: string;
    area?: Area;
    rol?: Rol;
}