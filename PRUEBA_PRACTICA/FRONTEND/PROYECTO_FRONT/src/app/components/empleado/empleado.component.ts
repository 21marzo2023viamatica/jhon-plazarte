import { Component } from '@angular/core';
import { EmpleadoService } from '../../services/Empleado/empleado.service';
import { Router } from '@angular/router';
declare var window: any;

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent {

  empleados:any[]=[];
  paginaActual:number=1;
  cantMostrar: number = 5;
  formModal: any;

  empleadoSeleccionado?:number;

  constructor(private apiEmpleado:EmpleadoService,private router:Router){
    this.listarEmpleados();
  }

  ngOnInit(): void {
    this.listarEmpleados();
    this.formModal = new window.bootstrap.Modal(
      document.getElementById('myModal')
    );
  }

  editarEmpleado(id:number) {
    this.empleadoSeleccionado=id;
    this.formModal.show();
  }

  listarEmpleados(){
    this.apiEmpleado.listar().subscribe(data=>{
      this.empleados=data;
    });
  }
  eliminaEmpleado(id:number){
    this.apiEmpleado.eliminar(id).subscribe(data => {
      console.log('deleted response', data);
      this.listarEmpleados();
    });
  }
}
