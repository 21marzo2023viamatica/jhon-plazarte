import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpleadoEditaComponent } from './empleado-edita.component';

describe('EmpleadoEditaComponent', () => {
  let component: EmpleadoEditaComponent;
  let fixture: ComponentFixture<EmpleadoEditaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmpleadoEditaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EmpleadoEditaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
