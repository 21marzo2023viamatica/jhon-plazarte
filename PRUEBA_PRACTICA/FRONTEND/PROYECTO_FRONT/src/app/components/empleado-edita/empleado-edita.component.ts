import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmpleadoService } from 'src/app/services/Empleado/empleado.service';
import { Empleado } from '../empleado-nuevo/Empleado';
import { Rol } from '../empleado-nuevo/Rol';
import { Area } from '../empleado-nuevo/Area';
import { AreaService } from 'src/app/services/Area/area.service';
import { RolService } from 'src/app/services/Rol/rol.service';
declare var window: any;

@Component({
  selector: 'app-empleado-edita',
  templateUrl: './empleado-edita.component.html',
  styleUrls: ['./empleado-edita.component.css']
})
export class EmpleadoEditaComponent {

  nombre?='';
  apellidos?='';
  fechaNace?=Date;
  correo?='';
  contrasena?='';
  rol?=1;
  area?=1;
  
  roles:any[]=[];
  areas:any[]=[];
  
  empleado: Empleado;
  unRol:Rol;
  unaArea:Area;

  constructor(private apiRol:RolService,private apiArea:AreaService,private router:Router,private apiEmpleado:EmpleadoService){
    this.empleado = new Empleado();
    this.unRol=new Rol();
    this.unaArea=new Area();
  }

  accion?:number;
  @Input() idEmpleado?: number;
  formModal: any;


  ngOnInit(): void {
      this.listarRoles();
      this.listarAreas();
      this.apiEmpleado.obtenerEmpleado(16).subscribe(
        data=>{
          this.empleado=data;
          this.nombre=this.empleado.nombre;
          this.apellidos=this.empleado.apellidos;
          this.correo=this.empleado.correo;
          this.rol=this.empleado.rol?.id;
          this.area=this.empleado.area?.id;
        }
      );

    this.formModal = new window.bootstrap.Modal(

      document.getElementById('myModal')
    );
  }
  

  saveSomeThing() {
    this.formModal.hide();
  }
  actualizaEmpleado(){
    console.log(this.accion);
  }
  listarAreas(){
    this.apiArea.listar().subscribe(data=>{
      this.areas=data;
    });
  }

  listarRoles(){
    this.apiRol.listar().subscribe(data=>{
      this.roles=data;
    });
  }
  
}
