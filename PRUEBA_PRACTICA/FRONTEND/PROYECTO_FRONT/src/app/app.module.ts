import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { EmpleadoComponent } from './components/empleado/empleado.component';
import { RouterModule } from '@angular/router';
import { EmpleadoService } from './services/Empleado/empleado.service';
import { HttpClientModule } from '@angular/common/http';
import { EmpleadoEditaComponent } from './components/empleado-edita/empleado-edita.component';
import { EmpleadoNuevoComponent } from './components/empleado-nuevo/empleado-nuevo.component';
import { RolService } from './services/Rol/rol.service';
import { AreaService } from './services/Area/area.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

const appRoutes=[
  {path:'login',component:LoginComponent},
  {path:'',component:EmpleadoComponent},
  {path:'empleadoNuevo',component:EmpleadoNuevoComponent},
  {path:'empleadoEdita/:id',component:EmpleadoEditaComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EmpleadoComponent,
    EmpleadoEditaComponent,
    EmpleadoNuevoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,ReactiveFormsModule,NgxPaginationModule
  ],
  providers: [EmpleadoService,RolService,AreaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
