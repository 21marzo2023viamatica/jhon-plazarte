package com.example.app.services;

import com.example.app.entities.Rol;

public interface RolSI {
	public Iterable<Rol> findAll();
}
