package com.example.app.services;

import com.example.app.entities.Area;

public interface AreaSI {
	public Iterable<Area> findAll();
}
