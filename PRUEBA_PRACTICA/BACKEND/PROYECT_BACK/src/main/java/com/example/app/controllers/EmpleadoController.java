package com.example.app.controllers;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.app.entities.Empleado;
import com.example.app.services.EmpleadoSI;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/empleado")
public class EmpleadoController {
	@Autowired
	private EmpleadoSI empleadoService;
	

	@GetMapping
	public List<Empleado> read(){
		List<Empleado> empleados=StreamSupport
				.stream(empleadoService.findAll().spliterator(), false)
				.collect(Collectors.toList());
		return empleados;
	}
	@GetMapping("/{id}")
	public ResponseEntity<?> read(@PathVariable Integer id){
		Optional<Empleado> oEmpleado=empleadoService.findById(id);
		if(!oEmpleado.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(oEmpleado);
	}
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Empleado empleado){
		return ResponseEntity.status(HttpStatus.CREATED).body(empleadoService.save(empleado));
	}
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody Empleado empleado,@PathVariable Integer id){
		Optional<Empleado> oEmpleado=empleadoService.findById(id);

		if(!oEmpleado.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		oEmpleado.get().setNombre(empleado.getNombre());
		oEmpleado.get().setApellidos(empleado.getApellidos());
		oEmpleado.get().setFechaNace(empleado.getFechaNace());
		oEmpleado.get().setCorreo(empleado.getCorreo());
		oEmpleado.get().setContrasena(empleado.getContrasena());
		oEmpleado.get().setArea(empleado.getArea());
		oEmpleado.get().setRol(empleado.getRol());
		return ResponseEntity.status(HttpStatus.CREATED).body(empleadoService.save(oEmpleado.get()));
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Integer id){
		if(!empleadoService.findById(id).isPresent()) {
			return ResponseEntity.notFound().build();
		}
		empleadoService.deleteById(id);
		return ResponseEntity.ok().build();
	}
}
