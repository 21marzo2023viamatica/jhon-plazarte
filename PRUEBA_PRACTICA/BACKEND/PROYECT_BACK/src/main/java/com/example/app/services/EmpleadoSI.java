package com.example.app.services;

import java.util.Optional;

import com.example.app.entities.Empleado;

public interface EmpleadoSI {
	public Iterable<Empleado> findAll();
	public Empleado save(Empleado empleado);
	public Optional<Empleado> findById(Integer id);
	public void deleteById(Integer id);
}
