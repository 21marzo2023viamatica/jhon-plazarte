package com.example.app.controllers;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.app.entities.Area;
import com.example.app.services.AreaSI;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/area")
public class AreaController {
	
	@Autowired
	private AreaSI areaService;
	

	@GetMapping
	public List<Area> read(){
		List<Area> areas=StreamSupport
				.stream(areaService.findAll().spliterator(), false)
				.collect(Collectors.toList());
		return areas;
	}
}
