package com.example.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.app.entities.Area;
import com.example.app.repository.AreaRepository;

@Service
public class AreaService implements AreaSI{
	
	@Autowired
	private AreaRepository areaRepository;
	

	@Override
	@Transactional(readOnly=true)
	public Iterable<Area> findAll() {
		return areaRepository.findAll();
	}
}
