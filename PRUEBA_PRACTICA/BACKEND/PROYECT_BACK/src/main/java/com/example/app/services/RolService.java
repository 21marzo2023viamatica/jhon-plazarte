package com.example.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.app.entities.Rol;
import com.example.app.repository.RolRepository;

@Service
public class RolService implements RolSI{
	
	@Autowired
	private RolRepository rolRepository;
	

	@Override
	@Transactional(readOnly=true)
	public Iterable<Rol> findAll() {
		return rolRepository.findAll();
	}
}
