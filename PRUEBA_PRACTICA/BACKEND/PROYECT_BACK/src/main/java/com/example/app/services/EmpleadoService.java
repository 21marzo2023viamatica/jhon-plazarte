package com.example.app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.app.entities.Empleado;
import com.example.app.repository.EmpleadoRepository;

@Service
public class EmpleadoService implements EmpleadoSI{
	
	@Autowired
	private EmpleadoRepository empleadoRepository;
	

	@Override
	@Transactional(readOnly=true)
	public Iterable<Empleado> findAll() {
		return empleadoRepository.findAll();
	}
	
	@Override
	@Transactional
	public Empleado save(Empleado empleado) {
		return empleadoRepository.save(empleado);
	}
	
	@Override
	@Transactional
	public Optional<Empleado> findById(Integer id) {
		return empleadoRepository.findById(id);
	}

	@Override
	public void deleteById(Integer id) {
		empleadoRepository.deleteById(id);
	}

}
