package com.example.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.app.entities.Empleado;

public interface EmpleadoRepository extends JpaRepository<Empleado,Integer>{

}
