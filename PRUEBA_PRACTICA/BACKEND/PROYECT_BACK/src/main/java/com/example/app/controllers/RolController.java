package com.example.app.controllers;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.app.entities.Rol;
import com.example.app.services.RolSI;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/rol")
public class RolController {
	
	@Autowired
	private RolSI rolService;
	

	@GetMapping
	public List<Rol> read(){
		List<Rol> roles=StreamSupport
				.stream(rolService.findAll().spliterator(), false)
				.collect(Collectors.toList());
		return roles;
	}
}
