# John Plazarte

## Ejecución
- Java 11 y spring boot 2.7.11 con maven
## Ejecución
### Crear Base de datos con en MySQL abriendo el script bdEmpleados.
1.	En MySQL seleccionar file.
2.	Seleccionar Open SQL script.
3.	Seleccionar el script bdEmpleados.
4.	Ejecutar Script.
### Spring Boot: debe abrir el proyecto con Spring Tool Suite 4
1.	Tener java 11.
2.	Reemplazar username y password de MySQL en el archivo application.properties de resources.
3.	Ejecutar proyecto.
4.	Abrir Postman.
5.	Abrir EmpleadosApi.postman_collection en Postman.
6.	Probar ejecuciones que contiene la colección.


## Librerias usadas
- jpa
- web
- devtools
- mysql-connector-j