CREATE DATABASE  IF NOT EXISTS `bdempleados` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `bdempleados`;
-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: bdempleados
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `area` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area`
--

LOCK TABLES `area` WRITE;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` VALUES (1,'Técnica'),(2,'Comercial'),(3,'Financiera');
/*!40000 ALTER TABLE `area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empleado` (
  `id` int NOT NULL AUTO_INCREMENT,
  `apellidos` varchar(255) DEFAULT NULL,
  `contrasena` varchar(255) DEFAULT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `fecha_nace` datetime(6) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `area_id` int DEFAULT NULL,
  `rol_id` int DEFAULT NULL,
  `eliminado` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKqtcqg9hsrfpq98djfgr00e1lr` (`area_id`),
  KEY `FKqudsrbctju4p60b7n0cdalf6d` (`rol_id`),
  CONSTRAINT `FKqtcqg9hsrfpq98djfgr00e1lr` FOREIGN KEY (`area_id`) REFERENCES `area` (`id`),
  CONSTRAINT `FKqudsrbctju4p60b7n0cdalf6d` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES (1,'Plazarte','123456','john@gmail.com','2000-03-24 00:00:00.000000','John',1,1,_binary ''),(2,'Plazarte','123456','jo@gmail.com','2000-03-24 00:00:00.000000','Ariel',1,1,_binary ''),(3,'','','',NULL,'',1,1,_binary ''),(4,'','','',NULL,'',1,1,_binary ''),(5,'Andrade','123456','m@gmail.com','2016-03-04 00:00:00.000000','maria',1,1,_binary ''),(6,'Muñoz','123456','v@gmail.com','2023-04-21 00:00:00.000000','Victor',2,2,_binary ''),(7,'Muñoz','123456','v@gmail.com','2023-04-14 00:00:00.000000','Victor',1,1,_binary ''),(8,'Muñoz','123456','v@gmail.com','2023-04-21 00:00:00.000000','Victor',1,1,_binary ''),(9,'Plazarte Suarez','123456','v@gmail.com','2023-03-30 00:00:00.000000','Victor',1,1,_binary ''),(10,'Plazarte','123456','jo@gmail.com','2000-03-24 00:00:00.000000','Ariel',1,1,_binary ''),(11,'Plazarte','123456','jo@gmail.com','2000-03-24 00:00:00.000000','Ariel',1,1,_binary ''),(12,'Plazarte','123456','jo@gmail.com','2000-03-24 00:00:00.000000','Ariel',1,1,_binary ''),(13,'Plazarte','123456','jo@gmail.com','2000-03-24 00:00:00.000000','Ariel',1,1,_binary ''),(14,'Muñoz','12345','v@gmail.com','2023-04-21 00:00:00.000000','Victor',1,1,_binary ''),(15,'Muñoz','123456789','v@gmail.com','2023-04-07 00:00:00.000000','Victor',1,1,_binary ''),(16,'Plazarte','123456','jo@gmail.com','2000-03-24 00:00:00.000000','Ariel',1,1,_binary '\0'),(17,'Plazarte','123456','jo@gmail.com','2000-03-24 00:00:00.000000','Ariel',1,1,_binary '\0'),(18,'Plazarte','123456','jo@gmail.com','2000-03-24 00:00:00.000000','Ariel',1,1,_binary '\0'),(19,'Plazarte','123456','jo@gmail.com','2000-03-24 00:00:00.000000','Ariel',1,1,_binary ''),(20,'Plazarte','123456','jo@gmail.com','2000-03-24 00:00:00.000000','Ariel',1,1,_binary ''),(21,'Plazarte','123456','jo@gmail.com','2000-03-24 00:00:00.000000','Ariel',1,1,_binary ''),(22,'Plazarte','123456','jo@gmail.com','2000-03-24 00:00:00.000000','Ariel',1,1,_binary ''),(23,'Plazarte','123456','jo@gmail.com','2000-03-24 00:00:00.000000','Ariel',1,1,_binary ''),(24,'Plazarte','123456','jo@gmail.com','2000-03-24 00:00:00.000000','Ariel',1,1,_binary ''),(25,'Cedeño Muñoz','123456','johnppp@gmail.com','2000-03-24 00:00:00.000000','Ariel',1,1,_binary ''),(26,'Cedeño','123456','johnppp@gmail.com','2000-03-24 00:00:00.000000','Ariel',1,1,_binary '\0'),(27,'Cedeño','123456','marcos@gmail.com','2000-03-24 00:00:00.000000','Marcos',2,2,_binary ''),(28,'Varela','123456','jvarela@gmail.com','2000-03-24 00:00:00.000000','Jefferson',2,2,_binary '\0'),(29,'Varela','123456','avarela@gmail.com','2000-03-24 00:00:00.000000','Anderson',2,2,_binary '\0'),(30,'Laaz','123456','kvarela@gmail.com','2000-03-24 00:00:00.000000','Kathia',3,2,_binary '\0'),(31,'SANTANA ZAMBRANO ','123456','cristina@uteq.edu.ec','2002-02-28 00:00:00.000000','MARIA CRISTINA',1,2,_binary '\0');
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (1,'Jefe'),(2,'Coordinador'),(3,'Operativo');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'bdempleados'
--

--
-- Dumping routines for database 'bdempleados'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-04-21 22:34:11
