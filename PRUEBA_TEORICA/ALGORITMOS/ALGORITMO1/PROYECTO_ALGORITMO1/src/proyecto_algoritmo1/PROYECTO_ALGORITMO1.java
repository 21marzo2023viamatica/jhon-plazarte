
package proyecto_algoritmo1;

import java.util.Scanner;

public class PROYECTO_ALGORITMO1 {

    /*
    1. Nike
    2. Adidas
    3. Puma
    4. Reebok
    5. Under Armour
    6. Bunky
    7. Venus
    8. Vans
    9. Fila
    10. Converse
    */
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Ingreso de máximo 5 números separados por comas");
        String cadena = reader.next();
        System.out.println(validarCadena(cadena));
    }
    //Funcion principal
    public static String validarCadena(String cadena){
        String[] marcas = cadena.split(",");
        if(marcas.length<=5 && marcas.length>1){
            if(Existeduplicados(marcas).equals("")){
                return formarCadena(marcas);
            }else{
                return Existeduplicados(marcas);
            }
            
            
        }else{
            return "El formato no es permitido";
        }
    }
    //Genera el resultado final cuando la cadena es valida
    public static String formarCadena(String[] marcas){
        String msg="";
        
        for(int x=0;x<marcas.length;x++){
            
            int nOpuesto=ObtenerNumeroOpuesto(Integer.parseInt(marcas[x])-1);
            
            for(int i=0;i<nOpuesto;i++){
                msg+=obtenerMarca(Integer.parseInt(marcas[x])-1)+"|";
            }
        }
        return msg;
    }
    //obtener numero de opuesto para duplicar esa cantidad de marca
    public static int ObtenerNumeroOpuesto(int n){
        int nOpuestos[]={10,9,8,7,6,5,4,3,2,1};
        return nOpuestos[n];
    }
    
    //revisar si existe 3 repetida para mostrar mensaje de que marca esta repetida
    public static String Existeduplicados(String [] marcas){
        int vRepetido=0;
        String msg="";
        for (int i = 0; i < marcas.length; i++)
        {
            for (int j = i + 1; j < marcas.length; j++)
            {
                if (marcas[i] != null && marcas[i].equals(marcas[j])) {
                    vRepetido++;
                }
            }
            if(vRepetido>=2){
                msg+="La marca "+obtenerMarca(Integer.parseInt(marcas[i])-1)+" se encuentra repetida "+(vRepetido+1)+" veces.";
                
            }
            vRepetido=0;
        }
        return msg;
    }
    public static String obtenerMarca(int n){
        String [] nombreMarcas={"Nike","Adidas","Puma","Reebok","Under Armour","Bunky","Venus","Vans","Fila","Converse"};
        return nombreMarcas[n];
    }
    
}
